-- fungsi succ ini akan mengembalikan bilangan atau huruf setelahnya

ghci> succ 8
9

-- fungsi min dan max akan mengembalikan nilai minimum jika min, dan mengembalikan nilai maksimum jika max
ghci> min 9 10
9
ghci> min 3.5 3.0
3.0
ghci> max 109 103
109

-- menggabungkan fungsi minmax dengan succ
ghci> succ 9 + max 5 4 + 1
-- (10 + 5 + 1)
16
ghci> (succ 9) + (max 5 4) +1
-- (10) + (5) + 1
16

-- fungsi div
ghci> div 92 10
9
ghci> div 4 2
2

ghci> 92 'div' 3
-- error karena quotation yang digunakan seharusnya ``
<interactive>:34:5: error:
    * Syntax error on 'div'
      Perhaps you intended to use TemplateHaskell or TemplateHaskellQuotes
    * In the Template Haskell quotation 'div'
ghci> 92 `div` 3
30