-- source: http://learnyouahaskell.com/starting-out#ready-set-go

-- fungsi untuk menjumlahkan dua nilai x
doubleEx x = x + x

--contohnya
-- ghci> doubleEx 4
-- 8

-- fungsi untuk menjumlahkan dua variable x dan y yang dikali dua
doubleExYe x y = x*2 + y*2 
-- fungsi di atas juga bisa ditulis dengan doubleExYe x y = x + x + y + y

-- contohnya
-- ghci> doubleExYe 6 2
-- 16

-- ghci> doubleEx 7 + doubleExYe 3 2
-- 24
-- cara penghitungannya = (7+7) + (3*2 + 2*2) = 14 + 6 + 4 = 24

-- fungsi yang memiliki maksud yang sama dengan fungsi diatas yaitu
-- untuk menjumlahkan dua bilangan a dan b yang sudah dikali dua
-- tetapi kali ini menggunakan fungsi yang telah ada sebelumnya
doubleExYe2 x y = doubleEx x + doubleEx y

-- contohnya
-- ghci> doubleExYe2 3 2
-- 10

-- fungsi yang memberikan hasil bilangan itu sendiri jika 
-- variable y > 100, akan tetapi akan memberikan hasil y*2
-- jika y <= 100
doubleSmallNumber y = if y > 100
                        then y
                        else y*2
-- contohnya
-- ghci> doubleSmallNumber 8
-- 16
-- ghci> doubleSmallNumber 101
-- 101
-- ghci> doubleSmallNumber 100
-- 200

-- fungsi yang mengalikan x dengan 2 kemudian menjumlahkannya dengan 1,
-- dalam kondisi jika x <= 100, akan tetapi jika x > 100, ia hanya akan
-- menjumlahkan x dengan 1
doubleSmallNumber' x = (if x > 100 then x else x*2) + 1
-- contohnya
-- ghci> doubleSmallNumber' 10
-- 21
-- ghci> doubleSmallNumber' 102
-- 103
-- ghci> doubleSmallNumber' 100
-- 201


-- fungsi untuk mencetak string yang di definisikan
conanO'Brien = "It's a-me, Conan O'Brien!"
-- hasilnya
-- ghci> conanO'Brien
-- "It's a-me, Conan O'Brien!"