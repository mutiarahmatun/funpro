-- materi tentang list comprehension

-- list coprehension in haskell
m = [1..5]

-- fungsi yang ingin menggantikan angka genap yang >= 10 dengan
-- tulisan "BANG!" dan yang lebih kecil dari 10 dengan "BOOM!"
boomBangs xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]
-- contohnya
-- ghci> boomBangs [6..9]
-- ["BOOM!","BOOM!"]
-- ghci> boomBangs [6..19]
-- ["BOOM!","BOOM!","BANG!","BANG!","BANG!","BANG!","BANG!"]

--fungsi yang digunakan untuk menghitung jumlah bilangan yang ada di list
length' xs = sum [1 | _ <- xs] 


-- Tuples
-- ghci> let triangles = [ (a,b,c) | c <- [1..10], b <- [1..10], a <- [1..10] ] 
-- fungsi diatas merupakan fungi yang menghasilkan semua kombinasi a,b,c