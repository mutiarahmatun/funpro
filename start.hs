GHCi, version 8.6.5: http://www.haskell.org/ghc/  :? for help
Prelude> :set prompt "ghci>"

-- source: http://learnyouahaskell.com/starting-out#ready-set-go

-- Starting out haskell --

--simple arithmetic
ghci>2+15
17
ghci>49 * 100
4900
ghci>1892 - 1472
420
ghci>5 / 2
2.5
ghci> (50 * 100) - 4999
1
ghci> 50 * 100 - 4999
1
ghci> 50 * (100 - 4999

-- error karena kurang tanda )
<interactive>:8:18: error:
    parse error (possibly incorrect indentation or mismatched brackets)
ghci> 50 * (100 - 4999)
-244950
ghci>

-- Boolean algebra

ghci> True && False
False
ghci> True && True
True
ghci> False || True
True
ghci> not False
True
ghci>not (True && True)
False