> primes = Sieve[2 .. ]
>    where
>        Sieve (x:xs) = x:Sieve[y|y<-xs, y 'mod' x /= 0]