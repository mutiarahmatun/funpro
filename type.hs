hapusHurufKecil :: [Char] -> [Char]
hapusHurufKecil st = [c | c <- st, c `elem` ['A'..'Z']]
-- CONTOHNYA
-- ghci> hapusHurufKecil ("aPa SalaHKU")
-- "PSHKU"


-- bahas contoh soal nomor 1 high order function
-- Define the length function using map and sum.
length :: [a] -> Int
length xs = sum (map (\x -> 1) xs)
-- contohnya
-- ghci> length' [1,2,3,4,5]
-- 5

-- Give the type of, and define the function iter so that
iter n f x = f (f (... (f x)))
-- where f occurs n times on the right-hand side of the equation. For instance, we should have
-- iter 3 f x = f (f (f x))
-- and iter 0 f x should return x.
iter :: Int -> (a -> a) -> (a -> a)
iter 0 f x = x
iter n f x = f (iter (n-1) f x)

--    iter n succ x = succ(succ (... (succ x)))
--    Artinya x ditambah 1 sebanyak n kali :
--        => x + n*1 = x + n
-- jadi, iter n succ x = x + n

-- type dari iter n succ x adalah 
-- n succ x :: Int -> (Num -> Num) -> (Num -> Num)
