-- test for equality
ghci> 5 == 5
True
ghci> 1 == 0
False
ghci> 5 /= 5
False
ghci> 5 /= 4
True

-- ternyata syntax di haskell tidak menerima petik satu 
ghci>'hello' == 'hello'

<interactive>:22:1: error:
    * Syntax error on 'hello'
      Perhaps you intended to use TemplateHaskell or TemplateHaskellQuotes
    * In the Template Haskell quotation 'hello'
ghci>"hello" == "hello"
True